<?php
    require '../vendor/autoload.php';

    use Google\Cloud\SecretManager\V1\SecretManagerServiceClient;

    if (strpos(getenv('SERVER_SOFTWARE'), 'Development') === 0) {
        $data = new stdClass();
        $projectId = 'deductive-span-313911';
        $versionId = 'latest';
        
        // Create the Secret Manager client.
        $client = new SecretManagerServiceClient();
    
        // Build the resource name of the secret version.
        $name = $client->secretVersionName($projectId, 'sql-hostname', $versionId);
        $response = $client->accessSecretVersion($name);
        $data->hostname = $response->getPayload()->getData();

        $name = $client->secretVersionName($projectId, 'sql-username', $versionId);
        $response = $client->accessSecretVersion($name);
        $data->username = $response->getPayload()->getData();

        $name = $client->secretVersionName($projectId, 'sql-password', $versionId);
        $response = $client->accessSecretVersion($name);
        $data->password = $response->getPayload()->getData();

        $name = $client->secretVersionName($projectId, 'sql-database', $versionId);
        $response = $client->accessSecretVersion($name);
        $data->database = $response->getPayload()->getData();
        return connect($data);
    } else {
        $xml = simplexml_load_file($_SERVER['DOCUMENT_ROOT'].'/private/config.xml');
        return connect($xml);
    }
    
    function get_config($config)
    {
        $xml = simplexml_load_file($config);
        return $xml;
    }

    function connect($xml)
    {
        global $output;
        
        $mysqli = new mysqli($xml->hostname, $xml->username, $xml->password, $xml->database);
        
        if ($mysqli->connect_error) {
            $output->error = "Connection failed: " . $mysqli->connect_error;
            echo json_encode($output);
            exit;
        }

        // this will make sure cyrilic letters are displayed properly
        $mysqli->query("SET NAMES utf8");

        return $mysqli;
    }
